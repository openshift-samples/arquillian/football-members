package com.example.footballmembers;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface  MembersRepository extends MongoRepository<Member, String> {

    Member findByName(String name);

    List<Member> findAll();


}
