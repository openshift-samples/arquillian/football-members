package com.example.footballmembers;

import org.springframework.data.annotation.Id;

public class Member {

    @Id
    private String id;

    private String name;
    private String position;
    private int kitNumber;

    public Member() {
    }

    public Member(String name, String position, int kitNumber) {
        this.name = name;
        this.position = position;
        this.kitNumber = kitNumber;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getKitNumber() {
        return kitNumber;
    }

    public void setKitNumber(int kitNumber) {
        this.kitNumber = kitNumber;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", kitNumber='" + kitNumber + '\'' +
                '}';
    }
}
