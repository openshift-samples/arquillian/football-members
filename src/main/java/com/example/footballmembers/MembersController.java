package com.example.footballmembers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/")
public class MembersController {

    @Autowired
    private MembersRepository repository;

    @RequestMapping("/")
    String index() {
        return "hello dude !!!";
    }

    @RequestMapping("/health")
    String health() {
        return "UP";
    }

    @RequestMapping(value = "/footballmembers", method = RequestMethod.GET)
    public List<Member> getAllMembers() {
        return repository.findAll();
    }

    @RequestMapping(value = "/footballmembers/{name}", method = RequestMethod.GET)
    public Member getMember(@PathVariable String name) {
        return repository.findByName(name);
    }

    @RequestMapping(value = "/footballmembers/add", method = RequestMethod.POST)
    public Member addMembers(@RequestParam(value = "name") String name, @RequestParam(value = "position") String position , @RequestParam(value = "kit") Integer kit) {
        Member member = new Member(name,position, kit);
        repository.save(member);
        return member;
    }

}
