# Football Members App

Simple Spring Boot application that comunicates with a Mongo DB.

Arquillian is used to create the integration test "environment", by creating a new project and starting a Mongo DB test instance


## How to Run
    mvn clean integration-test
